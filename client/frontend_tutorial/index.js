var server_port = 65432;
var server_addr = "192.168.1.18";   // the IP address of your Raspberry PI

const net = require('net');





function move(dir) {
    console.log(dir);
    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
    });
    client.write(`${dir}`);
    // get the data from the server
    client.on('data', (data) => {
        var info = JSON.parse(data.toString());
        document.getElementById("cpuTemp").innerHTML = info.t;
        document.getElementById("powerLvl").innerHTML = info.p;
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });
}

// function greeting() {
//     // get the element from html
//     var name = document.getElementById("myName").value;
//     // update the content in html
//     document.getElementById("greet").innerHTML = "Hello " + name + " !";
//     // send the data to the server 
//     //to_server(name);
//     client();
// }

