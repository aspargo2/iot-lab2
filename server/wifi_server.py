import socket
from picar_4wd.pwm import PWM
from picar_4wd.adc import ADC
from picar_4wd.pin import Pin
from picar_4wd.motor import Motor
from picar_4wd.servo import Servo
from picar_4wd.ultrasonic import Ultrasonic 
from picar_4wd.speed import Speed
from picar_4wd.filedb import FileDB  
from picar_4wd.utils import *
import picar_4wd as fc
import time

# Config File:
config = FileDB("~/.picar-4wd/config")
left_front_reverse = config.get('left_front_reverse', default_value = False)
right_front_reverse = config.get('right_front_reverse', default_value = False)
left_rear_reverse = config.get('left_rear_reverse', default_value = False)
right_rear_reverse = config.get('right_rear_reverse', default_value = False)    

ultrasonic_servo_offset = int(config.get('ultrasonic_servo_offset', default_value = 0)) 

speed = 5
turn_speed = 30

# Ultrasonic
ANGLE_RANGE = 100
STEP = 5
us_step = STEP
angle_distance = [0,0]
current_angle = 0
max_angle = ANGLE_RANGE/2
min_angle = -ANGLE_RANGE/2
scan_list = []

errors = []

servo = Servo(PWM("P0"), offset=ultrasonic_servo_offset)

# Init Ultrasonic
us = Ultrasonic(Pin('D8'), Pin('D9'))

HOST = "192.168.1.18" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)

def forward_step(direction):
    if direction == "f":
        fc.forward(speed)
        time.sleep(0.05)
        fc.stop()
    else:
        fc.forward(-speed)
        time.sleep(0.05)
        fc.stop()

def turn(direction):
    if direction == "r":
        fc.turn_right(turn_speed)
    else:
        fc.turn_left(turn_speed)
    time.sleep(1.1)
    fc.stop()

def power_read():
    from picar_4wd.adc import ADC
    power_read_pin = ADC('A4')
    power_val = power_read_pin.read()
    power_val = power_val / 4095.0 * 3.3
    # print(power_val)
    power_val = power_val * 3
    power_val = round(power_val, 2)
    return power_val

def cpu_temperature():          # cpu_temperature
    raw_cpu_temperature = subprocess.getoutput("cat /sys/class/thermal/thermal_zone0/temp")
    cpu_temperature = round(float(raw_cpu_temperature)/1000,2)               # convert unit
    #cpu_temperature = 'Cpu temperature : ' + str(cpu_temperature)
    return cpu_temperature

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()

    try:
        while 1:
            client, clientInfo = s.accept()
            print("server recv from: ", clientInfo)
            data = client.recv(1024)      # receive 1024 Bytes of message in binary format
            data = data.decode('ascii')
            if data != "":
                print(data)
                if data == "f" or data == "b":
                    forward_step(data)
                else:
                    turn(data)
                sdata = '{"p":'+str(power_read())+',"t":'+str(cpu_temperature())+'}'
                client.sendall(str.encode(sdata)) # Echo back to client
    except Exception as e:
        print(e)
        print("Closing socket")
        client.close()
        s.close()    

